# typing
from typing import Iterable, AnyStr

# dependencies
from gensim.utils import tokenize
from gensim.parsing.preprocessing import remove_stopwords


def preprocess(sentences: Iterable[AnyStr]) -> Iterable[Iterable[AnyStr]]:
    """
    Preprocess text for building and training Word2Vec model

    @param sentences: unprocessed sentences
    @type  sentences: Iterable[AnyStr]
    @return: 	preprocessed sentences (tokenized)
    @rtype: 	Iterable[Iterable[AnyStr]]

    """

    sentences_list = [preprocess_title(sent) for sent in sentences]

    return sentences_list


def preprocess_title(title: AnyStr) -> Iterable[AnyStr]:
    """
    Preprocess `title` string before training Word2Vec model

    @param title: unprocessed title
    @type  title: AnyStr
    @return:    processed `title` string for Word2Vec model training
    @rtype:     Iterable[AnyStr]

    """

    title = title.lower()

    # tokenize input (title strings -> words within title)
    title = list(tokenize(title))

    # remove stop words
    title = [remove_stopwords(word) for word in title]

    # remove numeric and short 'words'
    title = [word for word in title if word.isalpha() and len(word) > 1]

    return title
