import os
from dotenv import load_dotenv

load_dotenv('./.env')

# reddit account details
REDDIT_AUTH_READ_ONLY = {
    'client_id': os.getenv('REDDIT_CLIENT_ID'),
    'client_secret': os.getenv('REDDIT_CLIENT_SECRET'),
    'user_agent': os.getenv('REDDIT_AUTH_AGENT')
}

# reddit account user/pass
REDDIT_AUTH_USER = dict.copy(REDDIT_AUTH_READ_ONLY)
REDDIT_AUTH_USER['username'] = os.getenv('REDDIT_AUTH_USER')
REDDIT_AUTH_USER['password'] = os.getenv('REDDIT_AUTH_PASS')

# Reddit (PRAW) API Specific constants
ALLOWED_TIME_FILTERS = ['hour', 'day', 'week', 'month', 'year', 'all']
