# dependencies
from recommenders import filenaming, recommender
from recommenders.word2vec import create_vectors
from settings_reqqit import PARAM_IS_NOT_ALLOWED
import dataset
import numpy as np
import os
import reddit
import unittest

# modules to test
import reqqit


class TestReqqit(unittest.TestCase):

	@classmethod
	def setUpClass(cls) -> None:

		# collect saved model files from MODELS_PATH
		cls.model_files = reqqit.saved_model_files()

	def test_estimator_load(self):

		# load saved, pretrained estimator and word2vec model
		saved_model = recommender.estimator_load(self.model_files, 'RandomForestClassifier', verbose=True)
		self.assertIsInstance(saved_model, filenaming.SklearnWord2Vec)

		# get Reddit posts to recommend
		m_jobs, = reddit.multireddits(['jobs'])
		posts = m_jobs.top('day', limit=None)
		df_reddit = dataset.generate_dataset(posts)

		# make predictions on Reddit posts
		df_reqs = recommender.recommend(saved_model, df_reddit)
		print(df_reqs)

		pass

	def test_recommend_from_subreddit_not_a_valid_time(self):
		# ASSERT: passed param is one of the allowed time_filter values
		try:
			result = reqqit.recommend_from_subreddit(None, None)
		except Exception as e:
			self.assertEquals(str(e), PARAM_IS_NOT_ALLOWED)

	def test_recommend_from_posts(self):

		# get recent posts from jobs multireddit
		m_jobs,  = reddit.multireddits(['jobs'])		# unpack,
		posts = m_jobs.top('day', limit=None)
		posts_list = list(posts)		# convert from ListingGenerator to list

		# get recommendations
		post_reqs = reqqit.recommend_from_posts(posts_list)

		'''
			ASSERT correctness
		'''

		# load pretrained estimator and Word2Vec model
		saved_model = recommender.estimator_load(self.model_files)

		# get vector representations of recommendations
		reqs_titles = [req.title for req in post_reqs]
		df_reqs = dataset.generate_dataset(posts_list)
		reqs_vecs = create_vectors(reqs_titles, df_reqs, saved_model.word2vec)

		# load positive and negative training examples
		X_pos, X_neg = dataset.jobs_training_data(saved_model.word2vec)

		# analyze cosine similarity between vector representations of recommended posts and average of positive samples
		X_avg = np.mean(X_pos, axis=1)
		print('X_avg', X_avg)
		print('X_pos', X_pos[0, :])
		cosim = np.dot(X_avg, X_pos).sum()
		print(cosim)

		pass

	def test_saved_model_files(self):

		"""
			ASSERT: at least one saved model found and returned
		"""

		# count saved model files from MODELS_PATH (initialized in self.model_files)
		num_saved_models = len(self.model_files)
		self.assertTrue(num_saved_models > 1)

		"""
			ASSERT: only files (not directories) were returned
		"""

		are_files = [os.path.isfile(filepath) for filepath in self.model_files]
		self.assertTrue(all(are_files))


if __name__ == '__main__':
	unittest.main()
