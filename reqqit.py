# typing
from typing import AnyStr, Iterable, Optional, List
from praw.models import Submission, Subreddit, ListingGenerator

# module dependencies
from recommenders import filenaming
from recommenders.recommender import recommend, estimator_load
from settings_reqqit import MODELS_PATH
import dataset
import os
import reddit


def recommend_from_posts(posts: Iterable[Submission]) -> Iterable[Submission]:
	"""
	Recommend Submissions (posts) from subreddits within given `multireddit`

	@param posts: Submissions to consider for recommendation
	@type  posts: Iterable[praw.models.Submission]
	@return:	recommended Submissions
	@rtype:		Iterable[praw.models.Submission]

	"""

	# load saved, pretrained estimator and Word2Vec model
	saved_model_paths = saved_model_files()
	best_model_path = saved_model_paths[:1]		# they're sorted by highest test_score first in list
	saved_model = estimator_load(best_model_path)

	# make predictions
	posts_by_id = {p.id: p for p in posts}		# posts is a ListingGenerator; this keeps a copy for reuse after
	posts = list(posts_by_id.values())
	df_reddit = dataset.generate_dataset(posts)
	df_reqs = recommend(saved_model, df_reddit)

	# convert to Submissions (posts) to return
	post_reqs = [posts_by_id[row.id] for idx, row in df_reqs.iterrows() if row.y_pred == 1]

	return post_reqs


def recommend_from_subreddit(subreddit: Subreddit, period: AnyStr = 'all', limit: Optional[int] = None) -> ListingGenerator:
	"""
	Returns top Submissions (posts) from given `subreddit` based on Reddit's scoring/algorithm,
	for the given time `period` and `limit`

	Used in current daily email digests (digest.py) to generate recommendations.

	@param subreddit: Subreddit from which to fetch Submissions (posts)
	@type  subreddit: praw.models.Subreddit
	@param period: time period over which to fetch Submissions (valid time periods defined in ALLOWED_TIME_FILTERS)
	@type  period: AnyStr
	@param limit: control how many Submissions to fetch (None=all)
	@type  limit: None or int
	@return: 	"top" Submissions from the `subreddit` for the given time
	@rtype: 	praw.models.ListingGenerator

	"""

	reddit.assert_is_valid_period(period)

	return subreddit.top(time_filter=period, limit=limit)


def saved_model_files(models_path: str = MODELS_PATH) -> List[str]:
	"""
	Filter a directory listing of `models_path` to keep only files having expected saved model extension

	@param models_path: path to search in for valid saved model files
	@type  models_path: str
	@return:	all model files
	@rtype: 	List[str]

	"""

	# get a listing of candidate model file paths
	model_files_paths = [os.path.join(models_path, file) for file in os.listdir(models_path)
						 if os.path.isfile(os.path.join(models_path, file))]

	# apply filters
	filtered = [fp for fp in model_files_paths if not filenaming.filter_joblib(fp)]

	# sort alphabetically (which sorts by scoring by filenaming convention)
	filepaths = list(reversed(sorted(filtered)))

	return filepaths
