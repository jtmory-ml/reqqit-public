# reqqit

#### A personalized data-driven recommender system for reddit posts.

---
### Virtual environment set up

* Create a Python 3.8+ venv
* Activate it, then `pip install -r requirements.txt`

---
### Environment variables
* Create a file `.env` in project root (`/`) directory with the following structure and fields
* `settings_*.py` files read variables from `.env` and load/process them

```yaml
# reddit account info
REDDIT_CLIENT_ID='[created from Reddit account API access]'
REDDIT_CLIENT_SECRET='[created from Reddit account API access]'
REDDIT_AUTH_AGENT='a description of auth agent'
REDDIT_AUTH_USER='[Reddit account username]'
REDDIT_AUTH_PASS='[Reddit account password]'

# reqqit DATASET settings
DATASETS_PATH='./data/'
DATASETS_DELIMITER='~'
DATASETS_ENCODING='utf-8'

# reqqit MODELS settings
MODELS_PATH='./models/'

# reqqit digests
DIGEST_DAILY_MULTIREDDITS=[replace with comma-separated list of named multireddit (eg. jobs,sports)]
DIGEST_EMAIL_ADDRESS=[email address]
```

--- 
### Files and folders

* `dataset.py` — from subreddits or multireddits generate a DataFrame or .csv
* `reddit.py` — praw Python library integration layer
* `reqqit.py` — recommend posts from subreddits or multireddits

* `/data` — saved datasets in .csv format

* `/models` — saved models (Word2Vec, RandomForestClassifier, GradientBoostingClassifier)

* `/notebooks` — Jupyter notebooks:
    * `dataset_jobs.ipnyb` — for exploring Reddit jobs datasets
    * `model_tuning.ipnyb` — grid search model tuning of hyperparameters
    * `reddit_recommendations.ipnyb` — using pretrained estimator and Word2Vec models, make recommendations on recent Reddit posts
    * `training_pipeline.ipnyb` — pipeline for training Word2Vec model and ML models

* `/recommenders` — modules to generate recommendations:
    * `filenaming.py` — filenaming conventions and functions (load, save, filter, name model files)
    * `preprocess_text.py` — text preprocessing for Word2Vec models
    * `recommender.py` — makes recommendations on Reddit posts
    * `word2vec.py` — Word2Vec model methods
    
* `/tests` — unit tests; **need to be run from project root (`/`) directory**

#### Heroku deployment files
* `mailer.py` — sends emails using Mailgun on Heroku
* `digest.py` — script file to create a digest of recommended posts and sends an email; Heroku entry point
* `Procfile`, `runtime.txt` — Heroku deployment configuration files 

---
### Datasets

* Saved dataset files can be found in `/data/m/`
* Can be generated manually/on-demand from `tests/test_dataset.py::TestGenerateDataset::test_generate_dataset_multi_jobs`
