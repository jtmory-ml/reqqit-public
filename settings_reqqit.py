import os
from dotenv import load_dotenv

load_dotenv('./.env')

# Datasets folder
DATASETS_PATH = os.getenv('DATASETS_PATH')
DATASETS_PATH_MULTIREDDITS = os.path.join(DATASETS_PATH, 'm')
DATASETS_PATH_SUBREDDITS = os.path.join(DATASETS_PATH, 'r')
DATASETS_DELIMITER = str(os.getenv('DATASETS_DELIMITER'))
DATASETS_ENCODING = os.getenv('DATASETS_ENCODING')

# Models folder
MODELS_PATH = os.getenv('MODELS_PATH')

# Error messages
PARAM_IS_NOT_INSTANCE_OF = 'Passed parameter is not an instance of '
PARAM_IS_NOT_ALLOWED = 'Passed parameter is not one of the allowed time filters'
