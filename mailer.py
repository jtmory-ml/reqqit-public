import requests
from settings_mailer import *


def send_email(data: dict) -> requests.Response:
    """
    Send an email with Mailgun on Heroku
        data: dict fields expected, keys:
            'from': Email sender
            'subj': Email subject
            'html': Email HTML contents

    @param data:
    @type  data: dict
    @return:
    @rtype: requests.Response
    """

    response = requests.post(
        "https://api.mailgun.net/v3/{}/messages".format(MAILGUN_DOMAIN),
        auth=("api", MAILGUN_API_KEY),
        data={"from": "{} <mailgun@{}>".format(data['from'], MAILGUN_DOMAIN),
              "to": [DIGEST_EMAIL_ADDRESS],
              "subject": data['subj'],
              "html": data['html']})

    if not response.ok:
        response.raise_for_status()

    return response
