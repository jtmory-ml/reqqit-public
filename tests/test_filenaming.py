# typing
from typing import List

# dependencies
from recommenders.recommender import recommend
import os
import reqqit
import unittest

# module to test
from recommenders import filenaming


class TestFilenaming(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:

        # collect saved model files from MODELS_PATH
        cls.model_files = reqqit.saved_model_files()

    def test_estimator_filename_parser(self):

        def common_ASSERTs(tuples: List[filenaming.EstimatorFilename]):

            """	ASSERT: all same number were parsed and returned as there are total model files
            """

            self.assertTrue(len(tuples) == len(self.model_files))

        """
            ASSERT: if passed with a path, it's handled
        """

        tuples = [filenaming.estimator_filename_parser(fp) for fp in self.model_files]
        common_ASSERTs(tuples)

        """ 
            ASSERT: if passed without a path, it's handled
        """

        tuples = [filenaming.estimator_filename_parser(os.path.basename(fp)) for fp in self.model_files]
        common_ASSERTs(tuples)

    def test_estimator_filenames(self):

        # ASSUMED: at least one .joblib model is saved and tracked in git under MODELS_PATH
        filtered = filenaming.estimator_filenames(self.model_files)
        num_model = len(filtered)
        self.assertTrue(num_model >= 1)


class TestFilters(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:

        # collect saved model files from MODELS_PATH
        cls.model_files = reqqit.saved_model_files()

    def test_filter_filenames(self):

        num_saved_models = len(self.model_files)

        """ 
            ASSERT: `estimator_name` filtering works
        """

        for estimator_name in ['RandomForestClassifier', 'GradientBoostingClassifier']:
            filtered = [fp for fp in self.model_files
                        if not filenaming.filter_filenames(fp, estimator_name=estimator_name)]
            self.assertTrue(len(filtered) < num_saved_models)

        """ 
            ASSERT: `scoring_name` filtering works
        """

        # for scoring_name 'accuracy' we expect all saved model files to be kept (not filtered)
        filtered = [fp for fp in self.model_files
                    if not filenaming.filter_filenames(fp, scoring_name='accuracy')]
        self.assertTrue(len(filtered) == num_saved_models)

        # for any other scoring_name, we expect no saved model files after filtering (them all out)
        filtered = [fp for fp in self.model_files
                    if not filenaming.filter_filenames(fp, scoring_name='not_a_name')]
        self.assertTrue(len(filtered) == 0)

        """ 
            ASSERT: `estimator_name` AND `scoring_name` filtering works together
        """

        pass

    def test_filter_joblib(self):

        """
            ASSERT: files without .joblib extension are filtered out
        """

        num_saved_models = len(self.model_files)
        model_files_copy = list(self.model_files)
        model_files_copy[0] += '.jpeg'		# append extension that isn't '.joblib'
        filtered = [fp for fp in model_files_copy if not filenaming.filter_joblib(fp)]
        self.assertTrue(len(filtered) == num_saved_models - 1)


if __name__ == '__main__':
    unittest.main()
