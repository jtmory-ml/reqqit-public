# deps
import unittest
from numpy.testing import assert_array_equal
import pandas as pd

# modules to test
import reddit
import dataset
from settings_reqqit import *


class TestGenerateDataset(unittest.TestCase):

	@staticmethod
	def assert_dataframes_equal(df1, df2):
		df1.fillna(value='', inplace=True)
		df2.fillna(value='', inplace=True)
		for column in df1.columns:
			a1 = df2.loc[:, column]
			a2 = df1.loc[:, column]
			assert_array_equal(a1, a2)

	def assert_saved_dataframe(self, df, save_path):
		# ASSERT: returned a DataFrame
		self.assertIsInstance(df, pd.DataFrame)

		# save as CSV; include delimiter in written filename, before .csv extension
		df.to_csv(save_path, sep=DATASETS_DELIMITER, header=True, encoding=DATASETS_ENCODING)

		# read from CSV; and compare to in-memory
		# ASSERT: from-disk (fd) and in-memory (im) DataFrames are equal
		fd = pd.read_csv(save_path, header=0, sep=DATASETS_DELIMITER, encoding=DATASETS_ENCODING)
		self.assert_dataframes_equal(df, fd)

	def test_generate_dataset_from_subreddit(self):
		ro = reddit.access_read_only()
		sub = 'crostini'
		df = dataset.generate_dataset_from_subreddit(ro.subreddit(sub))

		# ASSERT: saved Dataframe correctly; from-disk and in-memory DataFrames are equal
		# save as CSV; include delimiter in written filename, before .csv extension
		save_file = '{}.{}.csv'.format(sub, DATASETS_DELIMITER)
		save_path = os.path.join(DATASETS_PATH_SUBREDDITS, save_file)
		self.assert_saved_dataframe(df, save_path)

	def test_generate_dataset_multi_jobs(self, period='week', limit=100):
		# get just 'jobs' multireddits
		jobs = reddit.multireddits(['jobs'])
		m_jobs = jobs[0]

		# get top posts from multireddit within time period and limit
		period = period if period in ['all', 'day', 'hour', 'month', 'week', 'year'] else 'week'
		posts = m_jobs.top(period, limit=limit)     # limit=None to retrieve all within period

		# create a dataset from those posts
		df = dataset.generate_dataset(posts)

		# ASSERT: saved Dataframe correctly; from-disk and in-memory DataFrames are equal
		# save as CSV; include delimiter in written filename, before .csv extension
		save_file = '{}.{}.{}.csv'.format(period, m_jobs.name, DATASETS_DELIMITER)
		save_path = os.path.join(DATASETS_PATH_MULTIREDDITS, save_file)
		self.assert_saved_dataframe(df, save_path)

	def test_generate_dataset_saved_jobs(self):
		# get just 'jobs' multireddits
		jobs = reddit.multireddits(['jobs'])
		m_jobs = jobs[0]
		saved_by_multi = reddit.saved_by_multireddits(jobs)

		# create a dataset from those saved items
		df = dataset.generate_dataset(saved_by_multi[m_jobs])

		# ASSERT: saved Dataframe correctly; from-disk and in-memory DataFrames are equal
		# save as CSV; include delimiter in written filename, before .csv extension
		save_file = 'saved.{}.{}.csv'.format(m_jobs.name, DATASETS_DELIMITER)
		save_path = os.path.join(DATASETS_PATH_MULTIREDDITS, save_file)
		self.assert_saved_dataframe(df, save_path)

	def test_remove_crossposts(self):
		# get 'jobs' multireddit
		jobs = reddit.multireddits(['jobs'])
		m_jobs = jobs[0]

		# get top posts from multireddit within time period
		period = 'day'
		posts = m_jobs.top(time_filter=period, limit=None)
		deduped, posts = dataset.remove_crossposts(posts)

		# assuming the posts within the given period has a
		# minimum of one crosspost this assertion should hold true
		assert len(deduped) < len(posts)

	def test_save_subreddit_dataset(self):
		# select 'crostini' subreddit
		sub = reddit.access_read_only().subreddit('crostini')

		# save subreddit dataset
		save_path = dataset.save_subreddit_dataset(sub)

		# ASSERT: saved Dataframe correctly; from-disk and in-memory DataFrames are equal
		df = dataset.generate_dataset_from_subreddit(sub)
		self.assert_saved_dataframe(df, save_path)


if __name__ == '__main__':
	unittest.main()
