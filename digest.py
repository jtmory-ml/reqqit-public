# local modules
import mailer
import reddit
import reqqit
from praw.models import Subreddit
from settings_digest import *


def create_digest_email(posts_grouped):
    def format_post(p):
        values = [p.shortlink, p.score, p.subreddit_name_prefixed, p.title]
        return u'<li><a href="{}">[{}] {} - {}</a></li>'.format(*values)

    html = [format_post(post) for group in posts_grouped for post in group]
    return u"\n<br/>\n".join(html).encode('utf-8')


def create_digest_content(subreddits_list, limit=5):
    # get recommended content (Submissions) from subreddits_list
    ro = reddit.access_read_only()
    return [list(reqqit.recommend_from_subreddit(Subreddit(ro, sub), period='day', limit=limit))
            for sub in subreddits_list]


# load multireddits' subreddits to digest from .env vars
print('DIGEST_DAILY_MULTIREDDITS', type(DIGEST_DAILY_MULTIREDDITS), DIGEST_DAILY_MULTIREDDITS)

# iterate over each multireddit listed
# generate and aggregate links from each multireddit into bodyHTML
bodyHTML = []
allLinks = []
ru = reddit.access_user()
multis = [multi for multi in reddit.multireddits() if multi.display_name in DIGEST_DAILY_MULTIREDDITS]
for multi in multis:
    # get recommended content (Submissions) from subreddits in multireddit
    multi_subs = reddit.multireddits_subreddits(multis=[multi])[0]
    multi_subs_names = [sub.display_name for sub in multi_subs]

    posts_grouped = create_digest_content(multi_subs_names, limit=5)
    posts_flatten = [post for group in posts_grouped for post in group]

    # create HTML email links from recommended content
    links = create_digest_email(posts_grouped)

    # wrap/group links in a <fieldset> tag
    multi_title = '{} [{}]'.format(multi.display_name, str(len(posts_flatten)))
    subs = "+".join(multi_subs_names)
    fieldset = "<fieldset><legend><b>{}</b></legend><ul>{}</ul><hr/>from: {}</fieldset><br/>".format(multi_title, links, subs)

    # aggregate HTML and links
    bodyHTML.append(fieldset)
    allLinks += posts_flatten

    print()
    print(multi.display_name, subs, posts_grouped)
    print(links)
    print()

# send a single email digest
name = 'reqqit'
data = {
    'html': "\n".join(bodyHTML),
    'subj': name + ' - ' + str(len(allLinks)),
    'from': name
}
resp = mailer.send_email(data)

print('data', data)
print('resp', resp)

# gracefully exit (so heroku won't restart/respawn)
exit(0)
