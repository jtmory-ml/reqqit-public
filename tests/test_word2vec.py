# dependencies
from recommenders.recommender import estimator_load
from reqqit import saved_model_files
from settings_reqqit import *
import unittest
import pandas as pd

# modules to test
from recommenders import word2vec


class TestWord2Vec(unittest.TestCase):

    def assert_jobs_words_in_vocab(self, model_vocab):
        # ASSERT: common words made it into vocabulary
        self.assertIn('hiring', model_vocab)
        self.assertIn('job', model_vocab)
        self.assertIn('remote', model_vocab)

        # ASSERT: tests bigram creation on a common phrase
        self.assertIn('machine_learning', model_vocab)

    def test_build_word2vec_model(self):
        # load all jobs data
        path_all = os.path.join(DATASETS_PATH, 'm', 'all.jobs.~.csv')
        df_all = pd.read_csv(path_all, header=0, sep=DATASETS_DELIMITER, encoding='utf-8', index_col=0)

        # build Word2Vec model
        wv_model = word2vec.build_word2vec_model(df_all.title.tolist(), {'min_count': 50, 'vector_size': 30})

        '''
            check for existence of several common words in model's vocabulary to ASSERT correctness
        '''

        # get model's vocabulary (as a list of words)
        model_vocab = word2vec.vocab(wv_model)

        # ASSERT: common words and bigrams found in job postings are in the model's vocab
        self.assert_jobs_words_in_vocab(model_vocab)

    def test_create_vectors(self):
        docs = ['[Hiring] 3D Computer Vision Researcher', '[Hiring] Applied Scientist - Computer Vision',
                '[Hiring] Senior Software Engineer, Data Science', '[HIRING] AI/ML- Machine Learning Engineer at Apple',
                '[Hiring] Senior Software Engineer - Machine Learning - Pleasanton Hub',
                '[Hiring] Engineering Manager: AI, Machine Learning, Computer Vision',
                '[Hiring] Machine Learning Scientist, Radiology',
                '[HIRING] AI/ML – Backend Software Engineer, Speech & Language Technologies at Apple',
                '[Hiring] Deep Learning Engineer',
                '[HIRING] NLP Research Scientist / Engineer (WeCNLP 2020) at Apple',
                '[Hiring] Software Engineer, Machine Learning & Big Data',
                '[Hiring] Deep Learning Engineer - Search and AI Research',
                '[HIRING] AI/ML- Software Engineer, Infrastructure at Apple',
                '[HIRING] Computer Vision Software Developer in Test at Apple',
                '[Hiring] Senior Software Engineer - Machine Learning',
                '[Hiring] Machine Learning Software Engineer',
                '[Hiring] Senior Software Engineer: Machine Learning Integration',
                '[Hiring] Software Development Engineer - ML / Big Data / Prime',
                '[HIRING] Deep Learning Algorithm Engineer, 3D Pose – Body Technologies at Apple',
                '[HIRING] Machine Learning Software Engineer at Apple',
                '[Hiring] Deep Learning Optimization Engineer',
                '[Hiring] Machine Learning Apps Engineer',
                '[Hiring] Data Scientist (Data Visualization) - Consumer Experience',
                '[HIRING] AI/ML Residency Program at Apple',
                '[HIRING] AI/ML – Software Engineer, Siri Search at Apple',
                '[HIRING] AI/ML – NLP/ML Scientist, Siri Understanding at Apple',
                '[Hiring] Computer Vision Software Engineer',
                '[HIRING] Staff Machine Learning Engineer – Personalized Listening Experiences at Spotify',
                '[Hiring] Software Engineer, Data Engineering',
                '[Hiring] Software Engineer in Machine Learning',
                '[Hiring] Computer Vision Software Engineer',
                '[Hiring] Staff Machine Learning Engineer',
                '[Hiring] Machine Learning Engineer (Computer Vision)']
        # wv_model = word2vec.build_word2vec_model()
        # TODO
        pass

    def test_vocab(self):

        # load saved model
        saved_model = estimator_load(saved_model_files())

        # get model's vocab
        model_vocab = word2vec.vocab(saved_model.word2vec)

        # ASSERT: common words and bigrams found in job postings are in the model's vocab
        self.assert_jobs_words_in_vocab(model_vocab)

