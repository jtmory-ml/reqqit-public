# typing
from typing import Tuple, Dict, Iterable, AnyStr, Callable
from pandas import DataFrame
from numpy import ndarray
from gensim.models import Word2Vec

# dependencies
import gensim
import numpy as np
from gensim.models.phrases import Phrases
from recommenders import preprocess_text
from recommenders.preprocess_text import preprocess_title


def bigrams(corpus: Iterable[Iterable[AnyStr]], **kwargs: Dict) -> Iterable[Iterable[AnyStr]]:
    """
    Given a `corpus` of sentences find bigrams and return corpus with bigrams

    @param corpus: the corpus from which to find phrases (bigrams)
    @type  corpus: Iterable[Iterable[AnyStr]]
    @param kwargs: kwargs to pass to Phrases() initialization
    @type  kwargs: Dict
    @return:    the corpus with bigrams
    @rtype:     Iterable[Iterable[AnyStr]]

    """

    # detect bigrams
    phrases = Phrases(corpus, **kwargs)

    # create corpus with bigrams
    corpus_bigrams = []
    for sentence in corpus:
        sentence_bigrams = phrases[sentence]
        corpus_bigrams.append(sentence_bigrams)

    return corpus_bigrams


def build_word2vec_model(corpus: Iterable[AnyStr], word2vec_kwargs: Dict = {}, bigrams_kwargs: Dict = {}) -> Word2Vec:
    """
    Preprocess documents in `corpus`. Find bigrams.
    Then build and train Word2Vec model using `corpus` with bigrams.

    @param corpus: the corpus for the Word2Vec language model to be trained on
    @type  corpus: Iterable[AnyStr]
    @param word2vec_kwargs: named parameters to pass to Word2Vec initialization
    @type  word2vec_kwargs: Dict
    @param bigrams_kwargs: named parameters to pass to Phrases initialization
    @type  bigrams_kwargs: Dict
    @return:    trained Word2Vec language model
    @rtype:     gensim.models.Word2Vec

    """

    # preprocess titles into sentences
    sentences_cleaned = preprocess_text.preprocess(corpus)

    # find bigrams
    sentences_bigrams = bigrams(sentences_cleaned, **bigrams_kwargs)

    # find trigrams (by passing in sentences with bigrams)
    # sentences_trigrams = bigrams(sentences_bigrams, min_count=min_count)

    # build and train model, create embeddings
    model = Word2Vec(sentences=sentences_bigrams, **word2vec_kwargs)

    return model


def create_vectors(docs: Iterable[AnyStr], df: DataFrame, model: Word2Vec) -> Tuple[ndarray, Iterable[AnyStr]]:
    """
    Create vector representations of `docs` from Word2Vec embeddings `model` .
    DataFrame `df` needed to gather which Submission IDs do not have vector representations with `model`.

    SOURCE: https://towardsdatascience.com/using-word2vec-to-analyze-news-headlines-and-predict-article-success-cdeda5f14751

    @param docs: documents to create vector representations from
    @type  docs: Iterable[AnyStr]
    @param df: has information about which id belongs to a post
    @type  df: pandas.DataFrame
    @param model: pretrained Word2Vec model
    @type  model: gensim.models.Word2Vec
    @return:    vector representations of `docs`, ids from `df` of `docs` without vector representations
    @rtype:     Tuple[numpy.ndarray, Iterable[AnyStr]]

    """

    # preprocess documents
    corpus = [preprocess_title(doc) for doc in docs]

    # 1. filter out docs that don't have any words in the model's vocab
    model_vocab = vocab(model)
    fn_cond = lambda doc: has_vector_representation(model_vocab, doc)
    docs_kept_after_filter, docs_list = filter_docs(corpus, docs, fn_cond)

    # 2. filter out empty documents too
    x = []
    docs_kept_after_filter, docs_list = filter_docs(docs_kept_after_filter, docs, lambda doc: (len(doc) != 0))

    # get ids of posts that were filtered to return them
    filtered_ids = [id for doc, title, id in zip(corpus, docs, df.id.tolist()) if doc not in docs_kept_after_filter]

    # for the documents kept after all filtering has been done, create vectors and return them
    for doc in docs_kept_after_filter:
        x.append(document_vector(model, doc))

    return np.array(x), filtered_ids


def create_vectors_filter_and_remove(df_to_filter: DataFrame, wv_model: Word2Vec) -> Tuple[ndarray, DataFrame]:
    """
    Create vector representations from `df_to_filter` `title` column using pretrained Word2Vec `wv_model`.
    Some rows in `title` with pretrained `wv_model` may not have vector representations and get filtered.
    Remove filtered rows from `df_to_filter` and return along with vector representations `X_filtered` as ndarray.

    SOURCE: https://towardsdatascience.com/using-word2vec-to-analyze-news-headlines-and-predict-article-success-cdeda5f14751

    @param df_to_filter: DataFrame to evaluate and filter/remove rows from
    @type  df_to_filter: pandas.DataFrame
    @param wv_model: pretrained Word2Vec model
    @type  wv_model: gensim.models.Word2Vec
    @return:    vector representations of `title` column from `df_to_filter`, corresponding filtered DataFrame
    @rtype:     Tuple[ndarray, pandas.DataFrame]

    """

    # create vector representations from dataframe titles;
    # some may be filtered, we'll remove them next
    X_temp, ids_filtered = create_vectors(df_to_filter.title.tolist(), df_to_filter, wv_model)

    # some rows may be filtered by create_vectors() call; remove them from the df
    if len(ids_filtered):
        df_to_filter = df_to_filter[~df_to_filter.id.isin(ids_filtered)]

    # also want to remove any rows that occur in the training/testing datasets
    # train_test_ids = df_positive.id.tolist() + df_negative.id.tolist()
    # df_to_filter = df_to_filter[~df_to_filter.id.isin(train_test_ids)]

    # now, with those rows removed, return output of calling create_vectors() again
    X_filtered, _ = create_vectors(df_to_filter.title.tolist(), df_to_filter, wv_model)

    return X_filtered, df_to_filter


def document_vector(word2vec_model: Word2Vec, doc: Iterable[AnyStr]) -> ndarray:
    """
    Create a vector representation of a document `doc` using pretrained `word2vec_model` by averaging all word vectors

    SOURCE: https://towardsdatascience.com/using-word2vec-to-analyze-news-headlines-and-predict-article-success-cdeda5f14751

    @param word2vec_model: pretrained Word2Vec model
    @type  word2vec_model: gensim.models.Word2Vec
    @param doc: document
    @type  doc: Iterable[AnyStr]
    @return:    vector representation of `doc`
    @rtype: numpy.ndarray

    """

    # remove out-of-vocabulary words
    word2vec_model_vocab = vocab(word2vec_model)
    doc = [word for word in doc if word in word2vec_model_vocab]

    # average word embeddings and return
    return np.mean(word2vec_model.wv[doc], axis=0)


def filter_docs(corpus: Iterable[AnyStr], texts: Iterable[AnyStr], condition_on_doc: Callable) -> Tuple[Iterable[AnyStr], Iterable[AnyStr]]:
    """

    Filter corpus and texts given the function condition_on_doc which takes a doc.
    The document doc is kept if condition_on_doc(doc) is true.

    SOURCE: https://towardsdatascience.com/using-word2vec-to-analyze-news-headlines-and-predict-article-success-cdeda5f14751

    @param corpus: documents to filter
    @type  corpus: Iterable[AnyStr]
    @param texts: when passed will be conditionally based along with associated document
    @type  texts: Iterable[AnyStr]
    @param condition_on_doc: callable function to evaluate whether to filter or keep each doc
    @type  condition_on_doc: function
    @return:    filtered collections of corpus and texts
    @rtype:     Tuple[Iterable[AnyStr], Iterable[AnyStr]]

    """

    number_of_docs = len(corpus)

    if texts is not None:
        texts = [text for (text, doc) in zip(texts, corpus) if condition_on_doc(doc)]

    corpus = [doc for doc in corpus if condition_on_doc(doc)]

    print("{} docs removed".format(number_of_docs - len(corpus)))

    return corpus, texts


def has_vector_representation(sentence: AnyStr, word2vec_model_vocab: Iterable[AnyStr]) -> bool:
    """
    Test to determine if at least one word of `sentence` is in the model's vocabulary

    SOURCE: https://towardsdatascience.com/using-word2vec-to-analyze-news-headlines-and-predict-article-success-cdeda5f14751

    @param sentence: sentence to test for a vector representation
    @type  sentence: AnyStr
    @param word2vec_model_vocab: Word2Vec model's vocabulary
    @type  word2vec_model_vocab: Iterable[AnyStr]
    @return:    whether there exists a vector representation for `sentence`
    @rtype:     bool

    """

    return not all(word not in word2vec_model_vocab for word in sentence)


# def load_saved_model() -> Word2Vec:
#     """
#     Load pretrained Word2Vec model saved to disk
#
#     @return:    pretrained Word2Vec model
#     @rtype:     gensim.models.Word2Vec
#
#     """
#
#     # load saved Word2Vec model (trained and saved along with the estimator)
#     _, wv_model = reqqit.estimator_load()
#
#     return wv_model


def visualize_vectors(X: DataFrame, df_titles: DataFrame, titles_to_plot: Iterable = list(np.arange(0, 400, 40))):
    """
    Visualize vector representations `X` generated from pretrained Word2Vec model.
    Annotate with titles `df_titles`. Which titles to plot is determine by the indices defined in `titles_to_plot`
    corresponding to

    SOURCE: https://towardsdatascience.com/using-word2vec-to-analyze-news-headlines-and-predict-article-success-cdeda5f14751

    @param X: vector representations generated from pretrained Word2Vec model
    @type  X: pandas.DataFrame
    @param df_titles: titles to use to annotate plot
    @type  df_titles: pandas.DataFrame
    @param titles_to_plot: indices of `X` and `df_titles` to annotate/label on plot
    @type  titles_to_plot: Iterable[int]

    """

    # Initialize t-SNE
    from sklearn.manifold import TSNE
    tsne = TSNE(n_components=2, init='random', random_state=10, perplexity=10)

    # Again use only 400 rows to shorten processing time
    tsne_df = tsne.fit_transform(X)  #[:400])

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(figsize=(14, 10))

    import seaborn as sns

    # set background color to `white`
    white = 'white'
    sns.set(rc={'axes.facecolor': white, 'figure.facecolor': white})
    sns.scatterplot(tsne_df[:, 0], tsne_df[:, 1], alpha=0.5)

    from adjustText import adjust_text
    texts = []

    # Append words to list
    titles_list = df_titles['title'].tolist()
    for title in titles_to_plot:
        texts.append(plt.text(tsne_df[title, 0], tsne_df[title, 1], titles_list[title], fontsize=14))

    # Plot text using adjust_text
    adjust_text(texts, force_points=0.4, force_text=0.4,
                expand_points=(2, 1), expand_text=(1, 2),
                arrowprops=dict(arrowstyle="-", color='black', lw=0.5))

    plt.show()


def vocab(model: Word2Vec) -> Iterable[AnyStr]:
    """
    Given a trained Word2Vec `model` return the vocabulary list

    @param model: trained Word2Vec model
    @type  model: gensim.models.Word2Vec
    @return:    the vocabulary of trained Word2Vec `model`
    @rtype:     Iterable[AnyStr]

    """

    model_vocab = list(model.wv.key_to_index)

    return model_vocab
