# typing
from gensim.models import Word2Vec
from numpy import ndarray
from pandas import DataFrame
from recommenders.filenaming import SklearnWord2Vec
from sklearn.base import BaseEstimator
from typing import AnyStr, List
import gensim.models
import sklearn.base

# dependencies
from joblib import load
from recommenders import filenaming
from recommenders.word2vec import create_vectors_filter_and_remove, vocab
import numpy as np
import pandas as pd


def estimator_load(filepaths: List[str], estimator_name: AnyStr = 'RandomForestClassifier',
                   scoring_name: AnyStr = 'accuracy', verbose: bool = False) -> SklearnWord2Vec:
    """
    Performs a search in `filepaths`

    @param filepaths: fullpath of filenames to search
    @type  filepaths: List[str]
    @param estimator_name: class name of estimator to match in filename
    @type  estimator_name: AnyStr
    @param scoring_name: scoring metric used for training model
    @type  scoring_name: AnyStr
    @param verbose: whether to print to console information about which models are loaded, and their params
    @type  verbose: bool
    @return: 	returns a single pretrained SklearnWord2Vec matching `estimator_name` and `scoring_name`
    @rtype: 	SklearnWord2Vec

    """

    # find filenames of pretrained models
    estimators_sorted_by_score = filenaming.estimator_filenames(filepaths, estimator_name, scoring_name)

    # select highest scoring (first one in sorted list) and load it
    path_best_estimator = estimators_sorted_by_score[0]
    saved_model = load(path_best_estimator)

    if verbose:
        estimator, word2vec = saved_model.estimator, saved_model.word2vec
        print(path_best_estimator)
        print(estimator.__class__.__name__ + "=\n\t", estimator.get_params())
        print('Word2Vec=\n\t', {'min_count': word2vec.min_count, 'vector_size': word2vec.vector_size})
        wv_vocab = vocab(word2vec)
        print('\t', {'vocab_size': len(wv_vocab), 'vocab': wv_vocab})

    return saved_model


def evaluate_performance(y_pred: ndarray, y_test: ndarray):
    """
    Get classification report and confusion matrix from predictions `y_pred` compared against truth `y_test`

    @param y_pred: predicted target labels
    @type  y_pred: numpy.ndarray
    @param y_test: true target labels
    @type  y_test: numpy.ndarray

    """

    target_labels = ['positive', 'negative']

    # classification report
    from sklearn.metrics import classification_report
    report_dict = classification_report(y_test, y_pred, target_names=target_labels, output_dict=True)
    print('Test Accuracy: \t', report_dict['accuracy'])
    print(report_dict)

    # confusion matrix
    import matplotlib.pyplot as plt
    from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
    conf_matrix = confusion_matrix(y_test, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=conf_matrix, display_labels=target_labels)
    disp.plot(cmap=plt.cm.Blues)
    plt.show()

    # TODO - identify which samples were misclassified

    # identify which samples were misclassified
    y_pred = y_pred.reshape(len(y_pred), 1)
    # y_test[:] != y_pred[:]

    pass


def recommend(trained_model: SklearnWord2Vec, df: DataFrame, verbose: bool = False) -> DataFrame:
    """
    Recommend using `estimator` Submissions (posts) from `df` with Word2Vec `wv_model`

    @param trained_model: trained classifier to use for predictions/recommendations
    @type  trained_model: recommenders.filenaming.SklearnWord2Vec
    @param df: Reddit Submissions (posts)
    @type  df: pandas.DataFrame
    @param wv_model: pretrained Word2Vec model
    @type  wv_model: gensim.models.Word2Vec
    @param verbose: conditionally print most-confident and least-confident recommendations
    @type  verbose: bool
    @return:    recommendations for Submissions (posts)
    @rtype:     pandas.DataFrame

    """

    # create vector representations using word embeddings
    X_reddit, df_reddit = create_vectors_filter_and_remove(df, trained_model.word2vec)

    # make predictions
    y_pred = trained_model.estimator.predict(X_reddit)  # predict binary classification target (0: negative, 1: positive)
    y_prob = trained_model.estimator.predict_proba(X_reddit)[:, 1]  # probability (confidence) of positive target (1)

    # aggregate predictions with identifying details (title, id)
    np_pred = np.column_stack([y_pred, y_prob, df_reddit.title, df_reddit.id])

    # put predictions into dataframe to sort by prediction probability
    df_pred = pd.DataFrame(data=np_pred, columns=['y_pred', 'y_prob', 'title', 'id']).sort_values('y_prob', axis=0, ascending=False)

    if verbose:
        # show most-confident "positive" predictions
        print(df_pred[:10])

        # show least-confident "positive" predictions
        print(df_pred[-10:])

    return df_pred
