# typing
from typing import Iterable


def apply_filter(iterable: Iterable, include: Iterable = (), exclude: Iterable = ()) -> Iterable:
    """
    Convenience method to filter `iterable` with white lists (`include`) OR (not and) black lists (`exclude`)

    @param iterable: 	input to be filtered
    @type  iterable: 	Iterable
    @param include: 	items to white list from input `iterable`
    @type  include: 	Iterable
    @param exclude: 	items to black list from input `iterable`
    @type  exclude: 	Iterable
    @return: 	filtered version of `iterable`
    @rtype: 	Iterable

    """

    incl = lambda x: x in include
    excl = lambda x: x not in exclude

    if len(include) == 0 and len(exclude) == 0:
        return iterable
    elif len(exclude) == 0:
        return list(filter(incl, iterable))
    elif len(include) == 0:
        return list(filter(excl, iterable))
