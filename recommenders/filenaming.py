# typing
from typing import AnyStr, Optional, Tuple, List, NamedTuple

# module dependencies
from collections import namedtuple
from dataclasses import dataclass
import gensim
import os
import sklearn

# define a namedtuple to establish sequence of parts that compose estimator filenames
from settings_reqqit import MODELS_PATH

# define namedtuple for estimator filenames; define which parts should be present, and in which order
EstimatorFilename = namedtuple('EstimatorFilename', ['test_score', 'scoring_name', 'estimator_name'])


# define @dataclass for saving/loading trained estimators/word2vec pairs
@dataclass(frozen=True)
class SklearnWord2Vec:
    estimator:  sklearn.base.BaseEstimator
    word2vec:   gensim.models.Word2Vec

    @staticmethod
    def file_ext() -> str:
        return '.joblib'


def estimator_filename_parser(filename: str, parts_sep: str = '.', score_sep: Tuple[str, str] = ('[', ']')):

    # if passed a path, remove it
    filename = os.path.basename(filename)

    # remove file extension
    filename_noext, ext = os.path.splitext(filename)

    # retrieve the score part
    _, removed_before = filename_noext.split(score_sep[0])
    removed_follow, filename_noscore = removed_before.split(score_sep[1])
    test_score = removed_follow

    # break file apart
    file_parts = filename_noscore.split(parts_sep)
    scoring_name, estimator_name = file_parts

    # return all parts together as NamedTuple
    return EstimatorFilename(test_score, scoring_name, estimator_name)


def estimator_filename_sequence(estimator_name: AnyStr, test_score: AnyStr, scoring_name: AnyStr) -> NamedTuple:
    """
    Determines a single source of truth for which order to sequence the parameters when constructing filenames.

    @param estimator_name: class name of estimator
    @type  estimator_name: AnyStr
    @param test_score: test score using scoring metric `scoring_name
    @type  test_score: AnyStr
    @param scoring_name: scoring metric name
    @type  scoring_name: AnyStr
    @return:	the single source of truth ordering of `estimator_name`, `test_score`, `scoring_name`
    @rtype:		NamedTuple

    """

    return EstimatorFilename(test_score, scoring_name, estimator_name)


def estimator_filename(estimator_name: AnyStr, test_score: float, scoring_name: AnyStr = 'accuracy') -> str:
    """
    Construct a filename to save a trained estimator and Word2Vec model

    @param estimator_name: class name of estimator
    @type  estimator_name: AnyStr
    @param test_score: test score using scoring metric `scoring_name
    @type  test_score: float
    @param scoring_name: scoring metric name
    @type  scoring_name: AnyStr
    @return: 	full path to model filename
    @rtype:		str

    """

    sequence = estimator_filename_sequence(str(test_score)[:6], scoring_name, estimator_name)
    filename = '[{}]{}.{}{}'.format(*sequence, SklearnWord2Vec.file_ext())
    fullpath = os.path.join(MODELS_PATH, filename)
    return fullpath


def estimator_filenames(filepaths: List[str], estimator_name: AnyStr = 'RandomForestClassifier',
                        scoring_name: AnyStr = 'accuracy') -> List[str]:
    """
    Performs a search in `filepaths` for filenames matching `recommender_name` and `scoring_name`

    @param filepaths: fullpath of filenames to search
    @type  filepaths: List[str]
    @param estimator_name: class name of estimator to match in filename
    @type  estimator_name: AnyStr
    @param scoring_name: scoring metric used for training model
    @type  scoring_name: AnyStr
    @return:	single fullpath of saved pretrained model matching `estimator_name` and `scoring_name`
    @rtype:		List[str]

    """

    # apply filters, then return
    filtered = [fp for fp in filepaths
                if not filter_filenames(fp, estimator_name, scoring_name) and not filter_joblib(fp)]

    return filtered


def filter_filenames(fullpath: str, estimator_name: Optional[str] = None, scoring_name: Optional[str] = None) -> bool:
    """
    Evaluate whether `fullpath` has filename matching with `recommender_name` and/or `scoring`
    True if it doesn't (True: DO filter it), False if it does (False: don't filter it).

    @param fullpath: full path to the candidate model file
    @type  fullpath: str
    @param scoring_name: name of the scoring method used
    @type  scoring_name: Optional[str]
    @param estimator_name: class name of the estimator
    @type  estimator_name: Optional[str]
    @return: 	True if it doesn't match `estimator_name` and/or `scoring_name`; False otherwise (it matches, keep it)
    @rtype:		bool

    """

    # if called with both default arguments (eg. None), there's nothing to do; early return False (keep it; no filters)
    if estimator_name is not None or scoring_name is not None:

        # parse filename into parts
        parsed_file = estimator_filename_parser(fullpath)

        # if estimator names don't match, filter out this file (by returning True)
        if estimator_name is not None and parsed_file.estimator_name.lower() != estimator_name.lower():
            return True

        # if scoring names don't match, filter out this file (by returning True)
        if scoring_name is not None and parsed_file.scoring_name.lower() != scoring_name.lower():
            return True

    # if none of the filters were matching, keep the file
    return False


def filter_joblib(fullpath: str) -> bool:
    """
    Evaluate whether `fullpath` has .joblib extension.
    True if it doesn't (True: DO filter it), False if it does (False: don't filter it).

    @param fullpath: full paths to candidate model files
    @type  fullpath: List[str]
    @return:	True if any extension besides '.joblib', False otherwise
    @rtype: 	bool
    """

    filename_no_ext, ext = os.path.splitext(fullpath)
    if ext.lower() != SklearnWord2Vec.file_ext().lower():
        return True

    # if none of the filters were matching, keep the file
    return False


