# dependencies
from numpy.testing import *
from reddit import access_read_only, multireddits, multireddits_subreddits

# modules to test
from utils.apply_filter import apply_filter


def setup_module(module):
    """ cache multireddits """
    multis_cached = multireddits()
    module.TestApplyFilter.multireddits = multis_cached
    module.TestApplyFilter.multireddits_subreddits = multireddits_subreddits(multis_cached)


class TestApplyFilter(TestCase):
    multireddits = []
    multireddits_subreddits = []

    def test_apply_filter(self):
        subs = [access_read_only().subreddit('nexus5x')]
        result = apply_filter(subs)
        assert_array_equal(result, subs)

    def test_apply_filter_exclude(self):
        exclude = [self.multireddits[0]]
        excluded = apply_filter(self.multireddits, exclude=exclude)
        TRUTH = list(self.multireddits)
        TRUTH.remove(exclude[0])
        assert_array_equal(TRUTH, excluded)

    def test_apply_filter_include(self):
        multis = self.multireddits
        include = [multis[0]]
        included = apply_filter(multis, include=include)
        assert_array_equal(include, included)
