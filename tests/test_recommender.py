# dependencies
from dataset import generate_dataset
from reddit import multireddits
from unittest import TestCase
import reqqit

# modules to test
from recommenders import recommender


class TestRecommender(TestCase):

    def test_recommend(self):

        # load pretrained estimator and word2vec model
        saved_model = recommender.estimator_load(reqqit.saved_model_files())

        # get Reddit posts to recommend
        m_jobs, = multireddits(['jobs'])
        posts = m_jobs.top('week', limit=None)
        df_reddit = generate_dataset(posts)

        # make recommendations
        df_recommended = recommender.recommend(saved_model, df_reddit)
        print(df_recommended)

        """        
            ASSERT: at least one recommended post
        """
        self.assertTrue(any(df_recommended.y_pred == 1))
