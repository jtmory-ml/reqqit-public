# typing
from typing import AnyStr, Dict, Iterable

# dependencies
import praw
from praw.models import *
from settings_reddit import *
from settings_reqqit import *


def access_read_only():
	"""
	Get an authenticated user with read-only access

	@return: 	read-only access authenticated user account
	@rtype: 	praw.Reddit

	"""

	return praw.Reddit(**REDDIT_AUTH_READ_ONLY)


def access_user():
	"""
	Get an authenticated user with full access

	@return: 	full-access authenticated user account
	@rtype: 	praw.Reddit

	"""

	return praw.Reddit(**REDDIT_AUTH_USER)


def assert_is_valid_period(period: AnyStr):
	"""
	Convenience method to assert whether passed `period` is within valid parameters as defined by ALLOWED_TIME_FILTERS

	@param period: 	time period (eg. 'day', 'week')
	@type  period: 	AnyStr

	"""

	if period not in ALLOWED_TIME_FILTERS:
		raise Exception(PARAM_IS_NOT_ALLOWED)


def comments_as_submissions(comments_or_posts: Iterable) -> Iterable[Submission]:
	"""
	Saved items from a user's account can be posts (Submission) or comments (Comment) on posts.
	This method takes these items as input and returns a list of Submission objects, returning
	the associated post for each Comment.

	@param comments_or_posts: 	user's saved items containing Comment and/or Submission instances;
								or a single Comment instance
	@type  comments_or_posts: 	Iterable or praw.models.Comment
	@return: 	returns a list of praw.models.Submission
	@rtype: 	Iterable[praw.models.Submission]

	"""

	if isinstance(comments_or_posts, Comment):
		return [comments_or_posts.submission]

	return [comment_or_submission.submission if isinstance(comment_or_submission, Comment)
			else comment_or_submission
			for comment_or_submission in comments_or_posts]


def multireddits(filter_by_name: Iterable[AnyStr] = ()) -> Iterable[Multireddit]:
	"""
	Will return a list of the authenticated user's Multireddits, optionally filtered by name

	@param filter_by_name: 	optionally, names of Multireddits to return
	@type  filter_by_name: 	Iterable[AnyStr]
	@return: 	authenticated user's Multireddits (optionally filtered by name)
	@rtype: 	Iterable[praw.models.Multireddit]

	"""

	all_user_multis = access_user().user.multireddits()

	if len(filter_by_name) == 0:
		return all_user_multis

	return [m for m in all_user_multis if m.name in filter_by_name or m.display_name in filter_by_name]


def multireddits_subreddits(multis: Iterable[Multireddit] = multireddits()) -> Iterable[Iterable[Subreddit]]:
	"""
	Given an iterable of Multireddits, returns a nested list of the Subreddits belonging to each Multireddit

	@param multis: 	specifies Multireddits to decompose into constituent Subreddits
	@type  multis: 	Iterable[praw.models.Multireddit]
	@return: 	nested list of the Subreddits belonging to each Multireddit
	@rtype: 	Iterable[Iterable[praw.models.Subreddit]]
	"""

	return [list(multireddit.subreddits) for multireddit in multis]


def saved(**kwargs) -> Iterable:
	"""
	Returns the authenticated user's saved items (posts and comments)

	@param kwargs: 	passthrough kwargs to praw API (eg. {'limit': None} to return all)
	@type  kwargs: 	Optional
	@return: 	user's saved items as list of praw.models.Submission and praw.models.Comment instances
	@rtype: 	Iterable

	"""

	username = REDDIT_AUTH_USER['username']
	return access_user().redditor(username).saved(**kwargs)


def saved_by_multireddits(multis: Iterable[Multireddit] = multireddits()) -> Dict:
	"""
	Returns the authenticated user's saved items (posts and comments),
	grouped by the Multireddit(s) to which they belong (if they do).

	@param multis: 	specifies which Multireddits to consider (default: all)
	@type  multis: 	Iterable[praw.models.Multireddit]
	@return: 	all user's saved items, grouped by the Multireddit to which they belong (if any)
	@rtype: 	Dict; keys: praw.models.Multireddit, values: lists of praw.models.Submission and praw.models.Comment

	"""

	# get list of all saved items for user
	items = list(saved(limit=None))

	# group saved items by multireddits
	saved_by_multi = {m: [] for m in multis}
	for i in items:
		for m in multis:
			if i.subreddit in m.subreddits:
				saved_by_multi[m] += [i]

	return saved_by_multi


def subscribed() -> Iterable[Subreddit]:
	"""
	Returns all the authenticated user's Subreddit subscriptions

	@return: 	all Subreddits to which the user has subscribed
	@rtype: 	Iterable[praw.models.Subreddit]

	"""

	return list(access_user().user.subreddits(limit=None))


def subreddits_universe(multis: Iterable[Multireddit] = multireddits_subreddits(), subs: Iterable[Subreddit] = subscribed()) -> Iterable[Subreddit]:
	"""
	Constructs a 'universe', an aggregation of all Subreddits belonging to the Multireddits passed as input,
	concatenated with the Subreddits

	@param multis:	Multireddits to consider when constructing 'universe'
	@type  multis: 	Iterable[praw.models.Multireddit]
	@param subs:	Subreddits to concatenate to `universe`
	@type  subs: 	Iterable[praw.models.Subreddit]
	@return:	all Subreddits passed plus those that belong to the passed Multireddits
	@rtype: 	Iterable[praw.models.Subreddit]

	"""

	return list(set([msub for msubs in multis for msub in msubs] + subs))
