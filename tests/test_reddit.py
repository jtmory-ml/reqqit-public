from numpy.testing import *
from praw import Reddit
from reddit import *


class TestAccess(TestCase):
    def test_access_read_only(self):
        instance = access_read_only()
        self.assertIsInstance(instance, Reddit)
        self.assertTrue(instance.read_only)

    def test_access_user(self):
        instance = access_user()
        self.assertIsInstance(instance, Reddit)
        self.assertFalse(instance.read_only)

    def test_multireddits_only(self):
        m_jobs = multireddits(['jobs'])
        assert len(list(m_jobs)) == 1


class TestCommentsAsSubmissions(TestCase):
    def test_comments_as_submissions(self):
        r = access_read_only()
        saved = [Comment(r, id='e560h95'), Comment(r, id='e4cpksx'), Comment(r, id='e463uej'),
                 Submission(r, id='8illzd'), Submission(r, id='8i6lre'), Submission(r, id='8737ey')]
        posts = comments_as_submissions(saved)
        for post in posts:
            self.assertIsInstance(post, Submission)


class TestMultireddits(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        # cache multireddits
        multis_cached = multireddits()
        cls.multireddits = multis_cached
        cls.multireddits_subreddits = multireddits_subreddits(multis_cached)

        # cache subscribed subreddits
        cls.subscribed = subscribed()

    multireddits = []
    multireddits_subreddits = []
    subscribed = []

    def test_multireddits(self):
        for multi in self.multireddits:
            self.assertIsInstance(multi, Multireddit)

    def test_multireddits_subreddits(self):
        for multi in self.multireddits_subreddits:
            for sub in multi:
                self.assertIsInstance(sub, Subreddit)

    def test_subscribed(self):
        subs = self.subscribed

        # assert what was returned is actually Subreddits
        for sub in subs:
            self.assertIsInstance(sub, Subreddit)

        # expect to have at a minimum one subreddit subscription
        assert len(subs)

    def test_subreddits_universe(self):
        subs = subreddits_universe(multis=self.multireddits_subreddits, subs=self.subscribed)

        # assert what was returned is actually Subreddits
        for sub in subs:
            self.assertIsInstance(sub, Subreddit)

        # expect to have at a minimum one subreddit subscription
        assert len(subs)


class TestSaved(TestCase):

    def test_saved(self):
        posts = saved()
        items = list(posts)
        self.assertTrue(len(items) == 100)

    def test_saved_limited(self):
        limit = 10
        posts = saved(limit=limit)
        items = list(posts)
        self.assertTrue(len(items) == limit)

    def test_saved_nolimit(self):
        posts = saved(limit=None)
        items = list(posts)
        self.assertTrue(len(items) > 100)
