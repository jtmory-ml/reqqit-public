import os
from dotenv import load_dotenv

load_dotenv('./.env')

# Mailgun settings
MAILGUN_API_KEY = os.getenv('MAILGUN_API_KEY')
MAILGUN_DOMAIN = os.getenv('MAILGUN_DOMAIN')

# Personal settings
DIGEST_EMAIL_ADDRESS = os.getenv('DIGEST_EMAIL_ADDRESS')
