import os
from dotenv import load_dotenv

load_dotenv('./.env')

# DAILY_DIGEST_SUBREDDITS;
# load value and parse
DAILY_DIGEST_SUBREDDITS = os.getenv('DAILY_DIGEST_SUBREDDITS')
expected_type = isinstance(DAILY_DIGEST_SUBREDDITS, basestring)
DAILY_DIGEST_SUBREDDITS = [] if not expected_type else DAILY_DIGEST_SUBREDDITS.split(',')

# DIGEST_DAILY_MULTIREDDITS;
# load value and parse
DIGEST_DAILY_MULTIREDDITS = os.getenv('DIGEST_DAILY_MULTIREDDITS')
expected_type = isinstance(DIGEST_DAILY_MULTIREDDITS, basestring)
DIGEST_DAILY_MULTIREDDITS = [] if not expected_type else DIGEST_DAILY_MULTIREDDITS.split(',')
