# typing
from typing import Tuple, Iterable, AnyStr, Dict
import numpy as np
from numpy import ndarray
from gensim.models import Word2Vec
from pandas import DataFrame
from praw.models import Submission, Subreddit

# dependencies
import datetime
import os
import pandas as pd
import reddit
import requests
from recommenders.word2vec import create_vectors
from settings_reqqit import *


def balance(a: ndarray, b: ndarray) -> Tuple[ndarray, ndarray]:
    """
    Balance dataframes a & b (when unbalanced) by discarding items in the larger of the two

    @param a: one distinct target (of 2) for binary classification task
    @type  a: numpy.ndarray
    @param b: one distinct target (of 2) for binary classification task
    @type  b: numpy.ndarray
    @return: 	balanced (even number of rows, instances) versions of `a` and `b`
    @rtype: 	Tuple[numpy.ndarray, numpy.ndarray]

    """

    # are they balanced already? early return if so
    if len(a) == len(b):
        return a, b

    if len(a) > len(b):
        return a[:len(b)], b

    # else:
    return a, b[:len(a)]


def download_html(link_href: AnyStr) -> AnyStr:
    """
    Download HTML contents of passed `link_href` (url)

    @param link_href: Submission field `url`
    @type link_href: AnyStr
    @return: 	HTML contents of passed link_href
    @rtype: 	AnyStr

    """

    try:
        r = requests.get(link_href)
    except requests.exceptions.MissingSchema:
        r = requests.get('https://reddit.com' + link_href)

    return r.text


def extract_object_detail(obj: Submission) -> Dict:
    """
    Creates a dict representation to return from the passed `obj`.
    Optionally, download HTML contents of url within passed `obj`.
    Future, handle Comment objects too

    @param obj:	Submission from which to extract details
    @type  obj: praw.models.Submission
    @return: 	a dict representation of the contents of the passed `obj`
    @rtype:		Dict

    """

    if isinstance(obj, Submission):
        '''		Submission (post) 	    '''
        return {
            'score': obj.score,
            'id': obj.id,
            'title': obj.title,
            'selftext': obj.selftext,
            'url': obj.url,
            'url_content': ''  # download_html(object.url)
        }

    # elif isinstance(object, praw.models.Comment):
    # 	"""		Comment 	"""
    # 	return {'title': object.link_title, 'url': object.link_url}

    return {}


def generate_dataset(items: Iterable[Submission]) -> DataFrame:
    """
    Given `items` create a DataFrame representation to return

    @param items: Reddit Submissions (posts)
    @type  items: Iterable[praw.models.Submission]
    @return: 	DataFrame representation of passed items
    @rtype: 	pandas.DataFrame

    """

    dataset = []
    created = []
    item_data = {}

    for i in items:
        dt = datetime.datetime.fromtimestamp(i.created_utc).strftime('%Y-%m-%d %H:%M:%S')
        item_data = extract_object_detail(i)
        dataset += [item_data.values()]
        created += [dt]

    # index: column 0 is datetime; Submission `created`
    df = pd.DataFrame(data=dataset, columns=item_data.keys(), index=created)
    return df


def generate_dataset_from_subreddit(subreddit: Subreddit, period: AnyStr = 'all', limit=None) -> DataFrame:
    """
    Creates and returns a DataFrame representations of Submissions (posts) from the passed `subreddit`,
    for the given time `period` and `limit`

    @param subreddit: the Subreddit from which to get `Top` Submissions (posts)
    @type  subreddit: praw.models.Subreddit
    @param period: any value defined within ALLOWED_TIME_FILTERS
    @type  period: AnyStr
    @param limit: controls how many Submissions (posts) to fetch
    @type  limit: None or int
    @return: 	DataFrame representation of posts within Subreddit
    @rtype: 	pandas.DataFrame

    """

    reddit.assert_is_valid_period(period)

    top = subreddit.top(time_filter=period, limit=limit)
    return generate_dataset(top)


def jobs_training_data(model: Word2Vec) -> Tuple[ndarray, ndarray]:
    """
    Loads saved positive and negative training samples for the `jobs` multireddit

    @param model: pretrained Word2Vec model to use to create vector representations to return
    @type  model: gensim.models.Word2Vec
    @return: 	positive and negative training samples as vector representations of Word2Vec embeddings
    @rtype:		Tuple[numpy.ndarray, numpy.ndarray]

    """

    # load positive training samples
    df_positive = load_csv(os.path.join(DATASETS_PATH, 'm', 'samples.positive.jobs.tsv'), separator='\t')
    X_positive, _filtered_ids = create_vectors(df_positive.title.tolist(), df_positive, model)

    # load negative training examples
    df_negative = load_csv(os.path.join(DATASETS_PATH, 'm', 'samples.negative.jobs.tsv'), separator='\t')
    X_negative, _filtered_ids = create_vectors(df_negative.title.tolist(), df_negative, model)

    # balance classes before returning
    X_positive, X_negative = balance(X_positive, X_negative)

    return X_positive, X_negative


def load_csv(path: AnyStr, separator: AnyStr = '~') -> DataFrame:
    """
    Reads a CSV file at `path` stored using delimiter `separator` and returns as DataFrame

    @param path: path to .csv file
    @type  path: AnyStr
    @param separator: delimiter/separator for .csv file
    @type  separator: AnyStr
    @return:	DataFrame of CSV from disk
    @rtype:		pandas.DataFrame

    """

    return pd.read_csv(path, header=0, sep=separator, encoding='utf-8', index_col=0)


def remove_crossposts(items: Iterable[Submission]) -> Tuple[Iterable[Submission], Iterable[Submission]]:
    """
    Given input of `items` remove duplicate Submissions (aka crossposts)

    @param items: iterable containing Submissions
    @type  items: Iterable[praw.models.Submission]
    @return:	the list of Submissions without crossposts and the list of all Submissions within `items`
    @rtype: 	Tuple[Iterable[praw.models.Submission], Iterable[praw.models.Submission]]

    """

    # pre-filter out any objects that aren't a Submission
    posts_only = [obj for obj in items if isinstance(obj, Submission)]

    crossposts = []
    for post in posts_only:
        dupes = list(post.duplicates())
        crossposts += dupes

    deduped = [p for p in posts_only if p not in crossposts]
    return deduped, posts_only


def remove_crossposts_from_dataframe(submission: Submission, dataframe: DataFrame) -> DataFrame:
    """
    Remove a Submission and all its duplicates (crossposts) from the passed DataFrame

    @param submission: Submission to consider
    @type  submission: praw.models.Submission
    @param dataframe: DataFrame from which to remove Submission(s)
    @type  dataframe: pandas.DataFrame
    @return: 	altered inplace DataFrame having `submission` and all of its crossposts removed
    @rtype: 	pandas.DataFrame

    """

    # retrieve submission's crossposts (duplicates)
    for dupe in submission.duplicates():
        dataframe.drop(dataframe.loc[dataframe['id'] == dupe.id].index, inplace=True)

    # remove the submission (post) itself from dataframe
    dataframe.drop(dataframe.loc[dataframe['id'] == submission.id].index, inplace=True)

    return dataframe


def save_subreddit_dataset(subreddit: Subreddit) -> AnyStr:
    """
    Given `subreddit` fetch posts and save as .CSV file

    @param subreddit: Subreddit from which to fetch Submissions (posts)
    @type  subreddit: praw.models.Subreddit
    @return:	fullpath and filename where .CSV file was saved
    @rtype:		AnyStr

    """

    # save as CSV; include delimiter in written filename, before .csv extension
    df = generate_dataset_from_subreddit(subreddit)
    save_file = '{}.{}.csv'.format(subreddit.display_name, DATASETS_DELIMITER)
    save_path = os.path.join(DATASETS_PATH_SUBREDDITS, save_file)
    delimiter = DATASETS_DELIMITER
    encoding = DATASETS_ENCODING
    df.to_csv(save_path, sep=delimiter, header=True, encoding=encoding)

    return save_path


def train_test_split(X_negative: ndarray, X_positive: ndarray, **kwargs) -> Tuple[ndarray, ndarray, ndarray, ndarray]:
    """
    Create train/test split of training samples (`negative` and `positive` in aggregate)

    @param X_negative: negative training samples
    @type  X_negative: numpy.ndarray
    @param X_positive: positive training samples
    @type  X_positive: numpy.ndarray
    @param kwargs: optional named arguments to pass into train_test_split (sklearn)
    @type  kwargs: Optional
    @return:    returns X_train, X_test, y_train, y_test after split
    @rtype:     Tuple[numpy.ndarray, numpy.ndarray, numpy.ndarray, numpy.ndarray]

    """

    X = np.vstack([X_negative, X_positive])
    y = np.vstack([
        np.zeros((len(X_negative), 1)),  # 0: negative
        np.ones((len(X_positive), 1)),  # 1: positive
    ])

    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1326, shuffle=True, **kwargs)

    return X_train, X_test, y_train, y_test
